import ctypes
from numpy.ctypeslib import ndpointer


num_chars_return = 42; #How many characters are expected as a response?

lib = ctypes.CDLL('./apdu.so')
lib.get_resp.restype = ndpointer(dtype=ctypes.c_char, shape=(num_chars_return,))

message = ctypes.c_char_p("\x00\xA4\x04\x00\x08\x01\x02\x03\x04\x05\x06\x07\x08") #query card
length = 13 #length of the message

res = lib.get_resp(message, length) #Returns array of characters

print ''.join(res)