import dbus
import dbus.mainloop.glib

try:
    from gi.repository import GObject
except ImportError:
    import gobject as GObject

from bluez_control_custom import *

mainloop = None

#def PollNFC():
def callKogon ():
    #Dummy input to simulate waiting on Kogon's value.
    Proceed = raw_input('To continue, type any value: ')
    return 1

class ManageTransaction ():

    def __init__(self):
        self.writeVal1 = 0
        self.writeVal2 = 0
        self.notifyVal1 = 0
        self.notifyVal2 = 0
        self.readVal1 = 0
        self.readVal2 = 0

    def transactionStep (self, encoding, value):
        if encoding == 1:
            return callKogon()
        elif encoding == 2:
            return 2
        elif encoding == 3:
            self.writeVal1 = value
            print repr(self.writeVal1)
        elif encoding == 4:
            return callKogon()
        elif encoding == 5:
            return 5
        elif encoding == 6:
            self.writeVal2 = value
            print repr(self.writeVal2)
            self.writeVal1 = 0
            self.writeVal2 = 0

class CommChrcNotify1(Characteristic):
    COMM_UUID = '12345678-1234-5678-1234-56789abc0001'

    def __init__(self, bus, index, service, tra_manager):
        Characteristic.__init__(
            self, bus, index,
            self.COMM_UUID,
            ['notify'],
            service)
        self.NFCvalue = 0
        self.timer = 0
        self.notifying = False
        self.manage = tra_manager
        GObject.timeout_add(500, self.checkNFC)

    def notifyNFCread(self):
        if not self.notifying:
            return
        print 'Notify 1 Transmitting'
        self.PropertiesChanged(
                GATT_CHRC_IFACE,
                { 'Value': [dbus.Byte(self.NFCvalue)] }, [])

    def checkNFC(self):
        if not self.notifying:
            return True
        self.timer = self.timer + 1
        if self.timer > 10:
            self.NFCvalue = self.manage.transactionStep(1, 0)
            self.timer = 0
        self.notifyNFCread()
        return True
        
    def StartNotify(self):
        if self.notifying:
            print('Already notifying, nothing to do')
            return

        self.notifying = True
        self.notifyNFCread()

    def StopNotify(self):
        if not self.notifying:
            print('Not notifying, nothing to do')
            return

        self.notifying = False

class CommChrcRead1(Characteristic):
    COMM_UUID = '12345678-1234-5678-1234-56789abc0010'

    def __init__(self, bus, index, service, tra_manager):
        Characteristic.__init__(
            self, bus, index,
            self.COMM_UUID,
            ['read'],
            service)
        self.value = 0
        self.manage = tra_manager

    def ReadValue(self, options):
        self.value = self.manage.transactionStep(2, 0)
        print ('Value = ' + repr(self.value))
        return [dbus.Byte(self.value)]

class CommChrcWrite1(Characteristic):
    COMM_UUID = '12345678-1234-5678-1234-56789abc0011'

    def __init__(self, bus, index, service, tra_manager):
        Characteristic.__init__(
            self, bus, index,
            self.COMM_UUID,
            ['write'],
            service)
        self.value = []
        self.manage = tra_manager

    def WriteValue (self, value, options):
        self.manage.transactionStep(3, value)
        #print ('Write ' + repr(value))
        self.value = value

class CommChrcNotify2(Characteristic):
    COMM_UUID = '12345678-1234-5678-1234-56789abc0100'

    def __init__(self, bus, index, service, tra_manager):
        Characteristic.__init__(
            self, bus, index,
            self.COMM_UUID,
            ['notify'],
            service)
        self.NFCvalue = 0
        self.timer = 0
        self.notifying = False
        self.manage = tra_manager
        GObject.timeout_add(500, self.checkNFC)

    def notifyNFCread(self):
        if not self.notifying:
            return
        print 'Notify 2 Transmitting'
        self.PropertiesChanged(
                GATT_CHRC_IFACE,
                { 'Value': [dbus.Byte(self.NFCvalue)] }, [])

    def checkNFC(self):
        if not self.notifying:
            return True
        self.timer = self.timer + 1
        if self.timer > 10:
            self.NFCvalue = self.manage.transactionStep(4, 0)
            self.timer = 0
        self.notifyNFCread()
        return True
        
    def StartNotify(self):
        if self.notifying:
            print('Already notifying, nothing to do')
            return

        self.notifying = True
        self.notifyNFCread()

    def StopNotify(self):
        if not self.notifying:
            print('Not notifying, nothing to do')
            return

        self.notifying = False

class CommChrcRead2(Characteristic):
    COMM_UUID = '12345678-1234-5678-1234-56789abc0101'

    def __init__(self, bus, index, service, tra_manager):
        Characteristic.__init__(
            self, bus, index,
            self.COMM_UUID,
            ['read'],
            service)
        self.value = 0
        self.manage = tra_manager

    def ReadValue(self, options):
        self.value = self.manage.transactionStep(5, 0)
        print ('Value = ' + repr(self.value))
        return [dbus.Byte(self.value)]

class CommChrcWrite2(Characteristic):
    COMM_UUID = '12345678-1234-5678-1234-56789abc0110'

    def __init__(self, bus, index, service, tra_manager):
        Characteristic.__init__(
            self, bus, index,
            self.COMM_UUID,
            ['write'],
            service)
        self.value = []
        self.manage = tra_manager

    def WriteValue (self, value, options):
        #ManageTransaction(6, value)
        #print ('Write ' + repr(value))
        self.manage.transactionStep(6, value)
        self.value = value

class CommService(Service):
    COMM_SVC_UUID = '12345678-1234-5678-1234-56789abc0001'

    def __init__(self, bus, index, tra_manager):
        Service.__init__(self, bus, index, self.COMM_SVC_UUID, True)
        self.add_characteristic(CommChrcNotify1(bus, 0, self, tra_manager))
        self.add_characteristic(CommChrcRead1(bus, 1, self, tra_manager))
        self.add_characteristic(CommChrcWrite1(bus, 2, self, tra_manager))
        self.add_characteristic(CommChrcNotify2(bus, 3, self, tra_manager))
        self.add_characteristic(CommChrcRead2(bus, 4, self, tra_manager))
        self.add_characteristic(CommChrcWrite2(bus, 5, self, tra_manager))
        #self.add_characteristic(CommChrc(bus, 0, self))
    
class CommApplication(Application):
    def __init__(self, bus, tra_manager):
        Application.__init__(self, bus)
        self.add_service(CommService(bus, 0, tra_manager))

class CommAdvertisement(Advertisement):
    def __init__(self, bus, index):
        Advertisement.__init__(self, bus, index, 'peripheral')
        self.add_service_uuid(CommService.COMM_SVC_UUID)
        self.add_local_name('PiAdvertiserECE')
        self.include_tx_power = True

def register_ad_cb():
    """
    Callback if registering advertisement was successful
    """
    print('Advertisement registered')


def register_ad_error_cb(error):
    """
    Callback if registering advertisement failed
    """
    print('Failed to register advertisement: ' + str(error))
    mainloop.quit()


def register_app_cb():
    """
    Callback if registering GATT application was successful
    """
    print('GATT application registered')


def register_app_error_cb(error):
    """
    Callback if registering GATT application failed.
    """
    print('Failed to register application: ' + str(error))
    mainloop.quit()

'''
def find_adapter(bus):
    remote_om = dbus.Interface(bus.get_object(BLUEZ_SERVICE_NAME, '/'),
                               DBUS_OM_IFACE)
    objects = remote_om.GetManagedObjects()

    for o, props in objects.items():
        if GATT_MANAGER_IFACE in props.keys():
            return o

    return None
'''

def main():
    global mainloop
    global testerVal

    dbus.mainloop.glib.DBusGMainLoop(set_as_default=True)

    bus = dbus.SystemBus()

    #adapter = find_adapter(bus)
    #if not adapter:
        #print 'LEAdvertisingManager1 interface not found'
        #return
    
    # Get ServiceManager and AdvertisingManager
    #service_manager = dbus.Interface(
            #bus.get_object(BLUEZ_SERVICE_NAME, adapter),
            #GATT_MANAGER_IFACE)

    service_manager = get_service_manager(bus)

    ad_manager = get_ad_manager(bus)

    tra_manager = ManageTransaction ()

    app = CommApplication(bus, tra_manager)

    commAd = CommAdvertisement(bus, 0)

    mainloop = GObject.MainLoop()

    service_manager.RegisterApplication(app.get_path(), {},
                                        reply_handler=register_app_cb,
                                        error_handler=register_app_error_cb)

    # Register advertisement
    ad_manager.RegisterAdvertisement(commAd.get_path(), {},
                                     reply_handler=register_ad_cb,
                                     error_handler=register_ad_error_cb)

    mainloop.run()
    '''
    try:
        mainloop.run()
    except KeyboardInterrupt:
        print testerVal
    '''

if __name__ == '__main__':
    main()
        
