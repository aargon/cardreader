# cardreader

Compile C with the command:

gcc -o apdu_query apdu_query.c -lnfc -shared -o apdu.so

The file get_resp.py shows how Python can query the card (minimal working example).

The file bluez_control_custom is the source functions required to run the bluetooth. The testing script actually controls the entire process.

How to Start the Application (BLE):
- make sure you are in the correct file 
- use: sudo systemctl restart bluetooth 
- use: python <filename>
 